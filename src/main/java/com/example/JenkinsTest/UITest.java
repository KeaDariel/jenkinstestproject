package com.example.JenkinsTest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Parameters;

public class UITest {

    @Parameters("Browser")
    @Test
    public void startBrowser(String browserName) {

        System.out.println("Parameter value is: " + browserName);
        WebDriver driver = null;

        if(browserName.contains("Chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }
        else if(browserName.contains("Edge")) {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }

        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/");
        Assert.assertTrue("Title does not match", driver.getTitle().contains("Orange"));
        driver.quit();

    }

}
